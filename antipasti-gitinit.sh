#!/bin/bash

#******************************************************************
#**
#**  antipasti git initialization script
#**
#******************************************************************
#** 
#** Copyright (C) 2009 Michael Lasmanis
#** 
#** Permission is hereby granted, free of charge, to any person obtaining
#** a copy of this software and associated documentation files (the 
#** "Software"), to deal in the Software without restriction, including
#** without limitation the rights to use, copy, modify, merge, publish,
#** distribute, sublicense, and/or sell copies of the Software, and to
#** permit persons to whom the Software is furnished to do so, subject to
#** the following conditions:
#** 
#** The above copyright notice and this permission notice shall be
#** included in all copies or substantial portions of the Software.
#** 
#** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
#** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#** 
#** Except as contained in this notice, the name of Michael Lasmanis
#** shall not be used in advertising or otherwise to promote the sale,
#** use or other dealings in this Software without prior written
#** authorization from him.
#** 
#******************************************************************

# what's my name
myname="${0##*/}"

# usage hints
function usage {
	echo "usage: ${myname} [-h | --help] [ --conffile CONFILE ] gitURL"
}

# some define
buildconffile="/usr/local/etc/antipasti/antipasti.conf"

# process args

giturl=""

while [ "$1" != "" ]; do
    case $1 in
		-h | --help )		usage
							exit 0
 							;;
		--conffile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --conffile" 
 								usage
 								exit 1
 							else
								buildconffile=$1
	 						fi
							;;
        * )					if [ -z "${giturl}" ]
							then
								giturl="$1"
							else
 								echo "${myname}: error: multiple gitURL's specified" 
 								usage
 								exit 1
							fi
	esac
    shift
done

# source the high level configuration
if [ -f "${buildconffile}" ]
then
	. "${buildconffile}"
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: Failed to load configuration file ${buildconffile}"
		exit 1
	fi
else
	echo "${myname}: error: Failed to locate configuration file ${buildconffile}"
	exit 1
fi

# check conf
confchecks="buildmktemppattern"
for confcheck in ${confchecks}
do
	if [ -z "${!confcheck}" ]
	then
		echo "${myname}: error: ${confcheck} not set in conffile"
		exit 1
	fi
done

# check binaries needed
binaries="which git mktemp rm touch head"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# make temp dir
tempdir=`mktemp -q -d -t "${buildmktemppattern}"`
if [ -z "${tempdir}" ]
then
	echo "${myname}: error: Failed to create temp dir"
	exit 1
fi

# cd to temp dir
if ! pushd "${tempdir}" > /dev/null 2>&1
then
	echo "${myname}: error: Failed to cd to ${tempdir}"
	exit 1
fi

# initialize git
if ! git init > /dev/null 2>&1
then
	echo "${myname}: error: git init failed"
	exit 1
fi

# create placeholder
if ! touch .placeholder
then
	echo "${myname}: error: failed to create placeholder file"
	exit 1
fi

# add it to git
if ! git add .placeholder > /dev/null 2>&1
then
	echo "${myname}: error: git add failed"
	exit 1
fi

# commit it
if ! git commit -m "initial commit" > /dev/null 2>&1
then
	echo "${myname}: error: git commit failed"
	exit 1
fi

# configure git
if ! git remote add antipasti ${giturl} > /dev/null 2>&1
then
	echo "${myname}: error: git remote add failed"
	exit 1
fi

# configure git
if ! git config remote.antipasti.push refs/heads/master:refs/heads/master ${gitURL} > /dev/null 2>&1
then
	echo "${myname}: error: git config failed"
	exit 1
fi

# git push
if ! git push antipasti master > /dev/null 2>&1
then
	echo "${myname}: error: git push failed"
	exit 1
fi

# remove the place holder
if ! git rm .placeholder > /dev/null 2>&1
then
	echo "${myname}: error: git rm failed"
	exit 1
fi

# commit it
if ! git commit -m "removed placeholder" > /dev/null 2>&1
then
	echo "${myname}: error: git commit failed"
	exit 1
fi

# git push
if ! git push antipasti master > /dev/null 2>&1
then
	echo "${myname}: error: git push failed"
	exit 1
fi

# pop out of the temp dir
if ! popd > /dev/null
then
	echo "${myname}: error: popd failed"
	exit 1
fi

# delete the temp dir
if ! rm -rf "${tempdir}"
then
	echo "${myname}: error: Failed to rm ${tempdir}"
	exit 1
fi

echo "Success"
exit 0