#!/bin/bash

#******************************************************************
#**
#**  antipasti package builder script
#**
#******************************************************************
#** 
#** Copyright (C) 2009 Michael Lasmanis
#** 
#** Permission is hereby granted, free of charge, to any person obtaining
#** a copy of this software and associated documentation files (the 
#** "Software"), to deal in the Software without restriction, including
#** without limitation the rights to use, copy, modify, merge, publish,
#** distribute, sublicense, and/or sell copies of the Software, and to
#** permit persons to whom the Software is furnished to do so, subject to
#** the following conditions:
#** 
#** The above copyright notice and this permission notice shall be
#** included in all copies or substantial portions of the Software.
#** 
#** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
#** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#** 
#** Except as contained in this notice, the name of Michael Lasmanis
#** shall not be used in advertising or otherwise to promote the sale,
#** use or other dealings in this Software without prior written
#** authorization from him.
#** 
#******************************************************************

# what's my name
myname="${0##*/}"

# usage hints
function usage {
	echo "usage: ${myname} [-h | --help] [--verbose] --repository URI --version VERSION [ --outdir path ] [ --conffile CONFILE ]"
}

# some define
buildconffile="/usr/local/etc/antipasti/antipasti.conf"

# process args

buildrepository=""
buildversion=""
buildoutdir=""
buildverbose=""

while [ "$1" != "" ]; do
    case $1 in
		-h | --help )		usage
							exit 0
 							;;
 		--verbose )			buildverbose="true"
 							;;
		--repository )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --repository" 
 								usage
 								exit 1
 							else
								buildrepository=$1
	 						fi
							;;
		--version )			shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --tag" 
 								usage
 								exit 1
 							else
								buildversion=$1
	 						fi
							;;
		--outdir )			shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --outdir" 
 								usage
 								exit 1
 							else
								buildoutdir=$1
	 						fi
							;;
		--conffile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --conffile" 
 								usage
 								exit 1
 							else
								buildconffile=$1
	 						fi
							;;
        * )					echo "error: unknown arg: $1"
							usage
							exit 1
	esac
    shift
done

# source the high level configuration
if [ -f "${buildconffile}" ]
then
	. "${buildconffile}"
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: Failed to load configuration file ${buildconffile}"
		exit 1
	fi
else
	echo "${myname}: error: Failed to locate configuration file ${buildconffile}"
	exit 1
fi

# check conf
confchecks="buildgitexportsubdir builddeboutputsubdir buildtagprefix"
for confcheck in ${confchecks}
do
	if [ -z "${!confcheck}" ]
	then
		echo "${myname}: error: ${confcheck} not set in conffile"
		exit 1
	fi
done

# check binaries needed
binaries="which git cmake make mktemp find mv chmod mkdir pwd rm fakeroot head"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# we need outdir
if [ -z "${buildoutdir}" ]
then
	buildoutdir=`pwd`
fi

# check for required args
if [ -z "${buildrepository}" -o -z "${buildversion}" -o -z "${buildoutdir}" ]
then
	echo "${myname}: error: missing required arg"
	usage
	exit 1
fi

# make the build tag
buildtag="${buildtagprefix}${buildversion}"

# make temp dir
echo "${myname}: info: Creating temp dir"
tempdir=`mktemp -q -d -t "${buildmktemppattern}"`
if [ -z "${tempdir}" ]
then
	echo "${myname}: error: Failed to create temp dir"
	exit 1
fi
echo "${myname}: info: Build directory: ${tempdir}"

# create the deb output dir
deboutputdir="${tempdir}/${builddeboutputsubdir}"
echo "${myname}: info: Creating output directory ${deboutputdir}"
if ! mkdir "${deboutputdir}" > /dev/null
then
	echo "${myname}: error: Failed to create dir"
	exit 1
fi

# cd into the temp tree
echo "${myname}: info: cd'ing to ${tempdir}"
if ! pushd "${tempdir}" > /dev/null
then
	echo "${myname}: error: Failed to pushd to temp dir"
	exit 1
fi

# extract the tag
exportdir="${buildgitexportsubdir}"
echo "${myname}: info: Cloning ${buildrepository}"
echo "${myname}: info:   into ${exportdir}"
if ! git clone "${buildrepository}" "${exportdir}" > /dev/null 2> /dev/null
then
	echo "${myname}: error: git clone failed"
	exit 1
fi

# cd into the export tree
echo "${myname}: info: cd'ing to ${exportdir}"
if ! pushd "${exportdir}" > /dev/null
then
	echo "${myname}: error: Failed to pushd to export dir"
	exit 1
fi

# move to our tag
echo "${myname}: info: Checking out version ${buildtag}"
if ! git checkout "${buildtag}" > /dev/null 2> /dev/null
then
echo "${myname}: error: git checkout failed"
exit 1
fi

# update submodules if present
if [[ -e .gitmodules ]]; then
	echo "${myname}: info: updating submodules"
	if ! git submodule init > /dev/null 2>&1; then
		echo "${myname}: error: git submodule init failed"
		exit 1
	fi
	if ! git submodule update > /dev/null 2>&1; then
		echo "${myname}: error: git submodule update failed"
		exit 1
	fi
fi

# check to make sure we have the right files
echo "${myname}: info: Checking for required files"
checkfiles="CMakeLists.txt"
for checkfile in ${checkfiles}
do
	if [ ! -f ${checkfile} ]
	then
		echo "${myname}: error: missing required file ${checkfile}"
		exit 1
	fi
done

# cmake
echo "${myname}: info: Running cmake"
if [ -n "${buildverbose}" ]
then
	if ! cmake -DANTIPASTI_VERSION=${buildversion} -DANTIPASTI_OUTPUT_DIR=${deboutputdir} .
	then
		echo "${myname}: error: cmake failed"
		exit 1
	fi
else
	if ! cmake -DANTIPASTI_VERSION=${buildversion} -DANTIPASTI_OUTPUT_DIR=${deboutputdir} . > /dev/null
	then
		echo "${myname}: error: cmake failed"
		exit 1
	fi
fi

# make
echo "${myname}: info: Running make"
if [ -n "${buildverbose}" ]
then
	if ! make
	then
		echo "${myname}: error: make package failed"
		exit 1
	fi
else
	if ! make > /dev/null
	then
		echo "${myname}: error: make package failed"
		exit 1
	fi
fi

# make package
echo "${myname}: info: Running make package"
if [ -n "${buildverbose}" ]
then
	if ! fakeroot make package
	then
		echo "${myname}: error: make package failed"
		exit 1
	fi
else
	if ! fakeroot make package > /dev/null
	then
		echo "${myname}: error: make package failed"
		exit 1
	fi
fi

# pop out of build dir
echo "${myname}: info: Popping out of build dir"
if ! popd > /dev/null
then
	echo "${myname}: error: Failed to pop out of build dir"
	exit 1
fi

# pop out of temp dir
echo "${myname}: info: Popping out of temp dir"
if ! popd > /dev/null
then
	echo "${myname}: error: Failed to pop out of temp dir"
	exit 1
fi

# locate any package files
echo "${myname}: info: Locating built packages"
debfiles=`find ${deboutputdir} -name "*.deb"`
if [ -z "${debfiles}" ]
then
	echo "${myname}: error: Failed to locate any built packages"
	exit 1
fi

# make sure permissions are right
echo "${myname}: info: Checking file permissions"
for debfile in ${debfiles}
do
	echo "${myname}: info: Setting permissions on ${debfile}"
	if ! chmod 444 "${debfile}" 2>/dev/null
	then
		echo "${myname}: error: Failed to chmod file: ${debfile}"
		exit 1
	fi
done

# move file to the final output dir
echo "${myname}: info: mv'ing packages to ${buildoutdir}"
for debfile in ${debfiles}
do
	echo "${myname}: info: mv'ing ${debfile}"
	if ! mv -f "${debfile}" "${buildoutdir}/" 2>/dev/null
	then
		echo "${myname}: error: Failed to mv file: ${debfile}"
		exit 1
	fi
done

# remove temp directory
echo "${myname}: info: rm'ing temp dir: ${tempdir}"
if ! rm -rf ${tempdir}
then
	echo "${myname}: error: Failed to rm dir: ${tempdir}"
	exit 1
fi

echo "${myname}: info: Done!"

exit 0
