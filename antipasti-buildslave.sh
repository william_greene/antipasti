#!/bin/bash

#******************************************************************
#**
#**  antipasti build slave script
#**
#******************************************************************
#** 
#** Copyright (C) 2009 Michael Lasmanis
#** 
#** Permission is hereby granted, free of charge, to any person obtaining
#** a copy of this software and associated documentation files (the 
#** "Software"), to deal in the Software without restriction, including
#** without limitation the rights to use, copy, modify, merge, publish,
#** distribute, sublicense, and/or sell copies of the Software, and to
#** permit persons to whom the Software is furnished to do so, subject to
#** the following conditions:
#** 
#** The above copyright notice and this permission notice shall be
#** included in all copies or substantial portions of the Software.
#** 
#** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
#** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#** 
#** Except as contained in this notice, the name of Michael Lasmanis
#** shall not be used in advertising or otherwise to promote the sale,
#** use or other dealings in this Software without prior written
#** authorization from him.
#** 
#******************************************************************

# what's my name
myname="${0##*/}"

# usage hints
function usage {
	echo "usage: ${myname} [-h | --help] [--verbose] --repository URI --version VERSION --outdir path [ --conffile CONFILE ]"
}

# some define
buildconffile="/usr/local/etc/antipasti/antipasti.conf"

# process args

buildrepository=""
buildversion=""
buildoutdir=""
buildverbose=""

while [ "$1" != "" ]; do
    case $1 in
		-h | --help )		usage
							exit 0
 							;;
 		--verbose )			buildverbose="true"
 							;;
		--repository )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --repository" 
 								usage
 								exit 1
 							else
								buildrepository=$1
	 						fi
							;;
		--version )			shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --tag" 
 								usage
 								exit 1
 							else
								buildversion=$1
	 						fi
							;;
		--outdir )			shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --outdir" 
 								usage
 								exit 1
 							else
								buildoutdir=$1
	 						fi
							;;
		--conffile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --conffile" 
 								usage
 								exit 1
 							else
								buildconffile=$1
	 						fi
							;;
        * )					echo "error: unknown arg: $1"
							usage
							exit 1
	esac
    shift
done

# source the high level configuration
if [ -f "${buildconffile}" ]
then
	. "${buildconffile}"
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: Failed to load configuration file ${buildconffile}"
		exit 1
	fi
else
	echo "${myname}: error: Failed to locate configuration file ${buildconffile}"
	exit 1
fi

# check conf
confchecks="ANTIPASTI_BIN"
for confcheck in ${confchecks}
do
	if [ -z "${!confcheck}" ]
	then
		echo "${myname}: error: ${confcheck} not set in conffile"
		exit 1
	fi
done

# check binaries needed
binaries="which mktemp find mv chmod head ${ANTIPASTI_BIN}/antipasti-buildpackage.sh"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# check for required args
if [ -z "${buildrepository}" -o -z "${buildversion}" -o -z "${buildoutdir}" ]
then
	echo "${myname}: error: missing required arg"
	usage
	exit 1
fi

# check the package output dir
outdir="${buildoutdir}"
if ! [ -d "${outdir}" ]
then
	echo "${myname}: error: target output directory does not exist: ${outdir}"
	exit 1
fi

# make temp dir
echo "${myname}: info: Creating temp dir"
tempdir=`mktemp -q -d -t "${buildmktemppattern}"`
if [ -z "${tempdir}" ]
then
	echo "${myname}: error: Failed to create temp dir"
	exit 1
fi

# info
echo "${myname}: info: Building ${buildrepository}"
echo "${myname}: info: Version: ${buildversion}"
echo "${myname}: info: Output Location: ${outdir}"
echo "${myname}: info: Intermediate Location: ${tempdir}"

# build it
buildcmd="${ANTIPASTI_BIN}/antipasti-buildpackage.sh"
if [ -n "${buildverbose}" ]
then
	buildcmd="${buildcmd} --verbose"
fi
buildcmd="${buildcmd} --repository ${buildrepository}"
buildcmd="${buildcmd} --version ${buildversion}"
buildcmd="${buildcmd} --outdir ${tempdir}"
buildcmd="${buildcmd} --conffile ${buildconffile}"

echo "${myname}: info: build command: ${buildcmd}"
if ! ${buildcmd}
then
	echo "${myname}: error: *****************"
	echo "${myname}: error: * BUILD FAILED! *"
	echo "${myname}: error: *****************"
	exit 1
fi

# cd into the temp tree
echo "${myname}: info: cd'ing to ${tempdir}"
if ! pushd "${tempdir}" > /dev/null
then
	echo "${myname}: error: Failed to pushd to temp dir"
	exit 1
fi

# locate the .deb files
debfiles=`find . -name "*.deb"`
if [ -z "${debfiles}" ]
then
	echo "${myname}: error: Failed to locate any built packages"
	exit 1
fi

# do we need to checksum the files
if [ -n "${buildmd5suffix}" ]
then
	# make sure permissions are right
	echo "${myname}: info: Generating checksums"
	for debfile in ${debfiles}
	do
		echo "${myname}: info: Working on ${debfile}"
		checksumfile="${debfile}${buildmd5suffix}"
		if ! md5sum "${debfile}" > "${checksumfile}"
		then
			echo "${myname}: error: Failed to checksum file: ${debfile}"
			exit 1
		fi
		if ! chmod 444 "${checksumfile}" 2>/dev/null
		then
			echo "${myname}: error: Failed to chmod file: ${checksumfile}"
			exit 1
		fi
	done
fi

# move the files to their final place
# make sure permissions are right
echo "${myname}: info: Delivering package files to ${buildoutdir}"
for debfile in ${debfiles}
do
	echo "${myname}: info: mv'ing ${debfile} to ${buildoutdir}"
	if ! mv "${debfile}" "${buildoutdir}" 2>/dev/null
	then
		echo "${myname}: error: Failed to mv file: ${debfile}"
		exit 1
	fi

	# are we also moving a checksum file
	if [ -n "${buildmd5suffix}" ]
	then
		checksumfile="${debfile}${buildmd5suffix}"
		echo "${myname}: info: mv'ing ${checksumfile} to ${buildoutdir}"
		if ! mv "${checksumfile}" "${buildoutdir}" 2>/dev/null
		then
			echo "${myname}: error: Failed to mv file: ${checksumfile}"
			exit 1
		fi
	fi
done

echo "${myname}: info: Done!"

exit 0
