#!/bin/bash

#******************************************************************
#**
#**  antipasti git checker script
#**
#******************************************************************
#** 
#** Copyright (C) 2009 Michael Lasmanis
#** 
#** Permission is hereby granted, free of charge, to any person obtaining
#** a copy of this software and associated documentation files (the 
#** "Software"), to deal in the Software without restriction, including
#** without limitation the rights to use, copy, modify, merge, publish,
#** distribute, sublicense, and/or sell copies of the Software, and to
#** permit persons to whom the Software is furnished to do so, subject to
#** the following conditions:
#** 
#** The above copyright notice and this permission notice shall be
#** included in all copies or substantial portions of the Software.
#** 
#** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
#** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#** 
#** Except as contained in this notice, the name of Michael Lasmanis
#** shall not be used in advertising or otherwise to promote the sale,
#** use or other dealings in this Software without prior written
#** authorization from him.
#** 
#******************************************************************

# what's my name
myname="${0##*/}"

# usage hints
function usage {
	echo "usage: ${myname} [-h | --help] [--verbose] [ --conffile CONFILE ] [ PATH ]"
}

# some define
buildconffile="/usr/local/etc/antipasti/antipasti.conf"

# process args

buildverbose=""
buildcheckpath=""

while [ "$1" != "" ]; do
    case $1 in
		-h | --help )		usage
							exit 0
 							;;
 		--verbose )			buildverbose="true"
 							;;
		--conffile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --conffile" 
 								usage
 								exit 1
 							else
								buildconffile=$1
	 						fi
							;;
        * )					if [ -z "${buildcheckpath}" ]
							then
								buildcheckpath="$1"
							else
 								echo "${myname}: error: multiple paths specified" 
 								usage
 								exit 1
							fi
	esac
    shift
done

# source the high level configuration
if [ -f "${buildconffile}" ]
then
	. "${buildconffile}"
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: Failed to load configuration file ${buildconffile}"
		exit 1
	fi
else
	echo "${myname}: error: Failed to locate configuration file ${buildconffile}"
	exit 1
fi

# check conf
confchecks="buildtagprefix"
for confcheck in ${confchecks}
do
	if [ -z "${!confcheck}" ]
	then
		echo "${myname}: error: ${confcheck} not set in conffile"
		exit 1
	fi
done

# check binaries needed
binaries="which git sed sort head wc cut grep"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# push into the dir
if [ -n "${buildcheckpath}" ]
then
	if ! pushd "${buildcheckpath}" > /dev/null
	then
		echo "${myname}: error: Failed to cd to ${buildcheckpath}"
		exit 1
	fi
fi

# find files with issues
if [ -n "${buildverbose}" ]
then
	echo "info: Checking git status"
fi
statuscheck=`git status 2> /dev/null`
errcode=$?
if [ ${errcode} -ne 0 -a ${errcode} -ne 1 ]
then
	echo "${myname}: error: git status command failed"
	exit 1
fi
statustest=`echo "${statuscheck}" | grep 'nothing to commit (working directory clean)'`
if [ -z "${statustest}" ]
then
	echo "${myname}: error: This source tree is not fully committed, please correct and try again."
	exit 1
fi

# make sure our tages are updated
if [ -n "${buildverbose}" ]
then
	echo "info: Fetching tags from repository"
fi
if ! git fetch --tags 2> /dev/null > /dev/null
then
	echo "${myname}: error: git fetch command failed"
	exit 1
fi
	 
# get the last tag
if [ -n "${buildverbose}" ]
then
	echo "info: Getting existing tags"
fi
tagset=`git tag 2>/dev/null | \
	sed -e 's/^'${buildtagprefix}'\([0-9][0-9\\.]*\)$/\1/p' -e 'd'`
if [ $? -ne 0 ]
then
	echo "${myname}: error: git tag command failed"
	exit 1
fi
lasttag=`echo "${tagset}" | \
	sort -t . -r -n --key=1,1 --key=2,2 --key=3,3 --key=4,4 --key=5,5 --key=6,6 --key=7,7 --key=8,8 --key=9,9 --key=10,10 | \
	head -1`
if [ $? -ne 0 ]
then
	echo "${myname}: error: sort command failed"
	exit 1
fi
if [ -z "${lasttag}" ]
then
	echo "${myname}: warning: Failed to locate existing tag"
else
	lasttag="${buildtagprefix}${lasttag}"

	# diff against the tag
	if [ -n "${buildverbose}" ]
	then
		echo "info: Diffing against tag ${lasttag}"
	fi
	tagdiffs=`git diff --name-only ${lasttag}`
	if [ -n "${tagdiffs}" ]
	then
		echo "${myname}: mismatch with tag ${lasttag}"
	else
		echo "${myname}: Latest = ${lasttag}"
	fi
fi

# pop out of the dir
if [ -n "${buildcheckpath}" ]
then
	if ! popd > /dev/null
	then
		echo "${myname}: error: Failed to pop out of ${buildcheckpath}"
		exit 1
	fi
fi

exit 0