#!/bin/bash

#******************************************************************
#**
#**  antipasti git tagger script
#**
#******************************************************************
#** 
#** Copyright (C) 2009 Michael Lasmanis
#** 
#** Permission is hereby granted, free of charge, to any person obtaining
#** a copy of this software and associated documentation files (the 
#** "Software"), to deal in the Software without restriction, including
#** without limitation the rights to use, copy, modify, merge, publish,
#** distribute, sublicense, and/or sell copies of the Software, and to
#** permit persons to whom the Software is furnished to do so, subject to
#** the following conditions:
#** 
#** The above copyright notice and this permission notice shall be
#** included in all copies or substantial portions of the Software.
#** 
#** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
#** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#** 
#** Except as contained in this notice, the name of Michael Lasmanis
#** shall not be used in advertising or otherwise to promote the sale,
#** use or other dealings in this Software without prior written
#** authorization from him.
#** 
#******************************************************************

# what's my name
myname="${0##*/}"

# usage hints
function usage {
	echo "usage: ${myname} [-h | --help] [--verbose] [ --conffile CONFILE ]"
}

# some define
buildconffile="/usr/local/etc/antipasti/antipasti.conf"

# process args

buildverbose=""

while [ "$1" != "" ]; do
    case $1 in
		-h | --help )		usage
							exit 0
 							;;
 		--verbose )			buildverbose="true"
 							;;
		--conffile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --conffile" 
 								usage
 								exit 1
 							else
								buildconffile=$1
	 						fi
							;;
        * )					echo "error: unknown arg: $1"
							usage
							exit 1
	esac
    shift
done

# source the high level configuration
if [ -f "${buildconffile}" ]
then
	. "${buildconffile}"
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: Failed to load configuration file ${buildconffile}"
		exit 1
	fi
else
	echo "${myname}: error: Failed to locate configuration file ${buildconffile}"
	exit 1
fi

# check conf
confchecks="buildtagprefix"
for confcheck in ${confchecks}
do
	if [ -z "${!confcheck}" ]
	then
		echo "${myname}: error: ${confcheck} not set in conffile"
		exit 1
	fi
done

# check binaries needed
binaries="which git sed sort head wc cut grep"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# find files with issues
echo "Checking git status"
statuscheck=`git status 2> /dev/null`
errcode=$?
if [ ${errcode} -ne 0 -a ${errcode} -ne 1 ]
then
	echo "${myname}: error: git status command failed"
	exit 1
fi
statustest=`echo "${statuscheck}" | grep 'nothing to commit (working directory clean)'`
if [ -z "${statustest}" ]
then
	echo "${myname}: error: This source tree is not fully committed, please correct and try again."
	exit 1
fi

# make sure our tages are updated
echo "Fetching tags from repository"
if ! git fetch --tags 2> /dev/null > /dev/null
then
	echo "${myname}: error: git fetch command failed"
	exit 1
fi
	 
# get the last tag
echo "Getting existing tags"
tagset=`git tag 2>/dev/null | \
	sed -e 's/^'${buildtagprefix}'\([0-9][0-9\\.]*\)$/\1/p' -e 'd'`
if [ $? -ne 0 ]
then
	echo "${myname}: error: git tag command failed"
	exit 1
fi
lasttag=`echo "${tagset}" | \
	sort -t . -r -n --key=1,1 --key=2,2 --key=3,3 --key=4,4 --key=5,5 --key=6,6 --key=7,7 --key=8,8 --key=9,9 --key=10,10 | \
	head -1`
if [ $? -ne 0 ]
then
	echo "${myname}: error: sort command failed"
	exit 1
fi
if [ -z "${lasttag}" ]
then
	echo "${myname}: warning: Failed to locate existing tag"
fi

# build new tag
if [ -n "${lasttag}" ]
then
	# how may version fields do we have
	numfields=`echo ${lasttag} | sed 's/[^\\.]//g' | wc -c | sed -e 's/ //g'`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: sed command failed"
		exit 1
	fi
	mainfields=$((${numfields} - 1))

	# increment the last field
	if [ "${mainfields}" -ne 0 ]
	then
		tagmajor=`echo "${lasttag}" | cut -d. -f-${mainfields}`
		tagminor=`echo "${lasttag}" | cut -d. -f${numfields}`
		tagminornext=$(( ${tagminor} + 1 ))
		nexttag="${tagmajor}.${tagminornext}"
	else
		nexttag=$(( ${lasttag} + 1 ))
	fi
else
	nexttag="1.0.0"
fi

echo " "
echo "Last Build Tag: ${lasttag}"
echo "Next Build Tag: ${nexttag}"
echo " "

# get the user input
echo -n "Enter new tag or return for default [${nexttag}]: "
read newtag
if [ -z "${newtag}" ]
then
	newtag=${nexttag}
fi

# check the requested tag
checktag=`echo ${newtag} | sed -e 's/^\([0-9][0-9\\.]*\)*[0-9]$/+++++/p' -e 'd'`
if [ $? -ne 0 ]
then
	echo "${myname}: error: sed command failed"
	exit 1
fi
if [ "${checktag}" != "+++++" ]
then
	echo "${myname}: error: bad version format -- please only use numbers and dashes."
	exit 1
fi

# get all the valid build tags
echo "Checking for duplicate tag"
if [ -n "${tagset}" ]
then
	for priortag in ${tagset}
	do
		if [ "${priortag}" = "${newtag}" ]
		then
			echo "${myname}: error: tag already exists!"
			exit 1
		fi
	done
fi

# build the new cvs tag
newtag="${buildtagprefix}${newtag}"
echo " "
echo "About to tag package with tag ${newtag}"
echo " "
echo -n "Please confirm with y/n [n]: "
read confirm
if [ "${confirm}" != "y" -a "${confirm}" != "Y" ]
then
	echo "${myname}: warning: Tag aborted"
	exit 0
fi

# tag
echo "Tagging... "
if ! git tag -a -m "tag for build ${newtag} via ${myname}" ${newtag} > /dev/null 2> /dev/null
then
	echo "${myname}: error: git tag failed"
	exit 1
fi

# force a push
echo "Pushing"
if ! git push 2> /dev/null > /dev/null
then
	echo "${myname}: error: git push failed"
	exit 1
fi

# push out the new tag
echo "Pushing new tag"
if ! git push --tags 2> /dev/null > /dev/null
then
	echo "${myname}: error: git push failed"
	exit 1
fi

# done
echo "Success"
exit 0