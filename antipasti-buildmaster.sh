#!/bin/bash

#******************************************************************
#**
#**  antipasti build master script
#**
#******************************************************************
#** 
#** Copyright (C) 2009 Michael Lasmanis
#** 
#** Permission is hereby granted, free of charge, to any person obtaining
#** a copy of this software and associated documentation files (the 
#** "Software"), to deal in the Software without restriction, including
#** without limitation the rights to use, copy, modify, merge, publish,
#** distribute, sublicense, and/or sell copies of the Software, and to
#** permit persons to whom the Software is furnished to do so, subject to
#** the following conditions:
#** 
#** The above copyright notice and this permission notice shall be
#** included in all copies or substantial portions of the Software.
#** 
#** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
#** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#** 
#** Except as contained in this notice, the name of Michael Lasmanis
#** shall not be used in advertising or otherwise to promote the sale,
#** use or other dealings in this Software without prior written
#** authorization from him.
#** 
#******************************************************************

# what's my name
myname="${0##*/}"

# usage hints
function usage {
	echo "usage: ${myname} [-h | --help] [--verbose] [ --conffile CONFILE ]"
}

# some define
buildconffile="/usr/local/etc/antipasti/antipasti.conf"

# process args

buildverbose=""

while [ "$1" != "" ]; do
    case $1 in
		-h | --help )		usage
							exit 0
 							;;
 		--verbose )			buildverbose="true"
 							;;
		--conffile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --conffile" 
 								usage
 								exit 1
 							else
								buildconffile=$1
	 						fi
							;;
        * )					echo "error: unknown arg: $1"
							usage
							exit 1
	esac
    shift
done

# source the high level configuration
if [ -f "${buildconffile}" ]
then
	. "${buildconffile}"
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: Failed to load configuration file ${buildconffile}"
		exit 1
	fi
else
	echo "${myname}: error: Failed to locate configuration file ${buildconffile}"
	exit 1
fi

# check conf
confchecks="ANTIPASTI_BIN buildtagprefix buildtargetsconf buildpackagesconf"
for confcheck in ${confchecks}
do
	if [ -z "${!confcheck}" ]
	then
		echo "${myname}: error: ${confcheck} not set in conffile"
		exit 1
	fi
done

# check binaries needed
binaries="which sed git sort ssh cat cut"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# did we build at least one package
buildminsuccess=""

# say hello
echo ""
echo "**************************************************"
echo "*"
echo "* Antipasti Build System"
echo "*"
echo "**************************************************"

# set paths to config files
conftargets="${buildtargetsconf}"
confpackages="${buildpackagesconf}"
if ! [ -f "${conftargets}" ]
then
	echo "${myname}: error: missing conf file: ${conftargets}"
	exit 1
fi
if ! [ -f "${confpackages}" ]
then
	echo "${myname}: error: missing conf file: ${confpackages}"
	exit 1
fi

# read the arch info
targetlist=""
targetchoices=""
while read line
do
	# remove comments and whitespace
	cleanline=`echo "${line}" | sed 's/^\([^#]*\)#.*$/\1/g' | sed 's/[ 	]//g'`

	# check the line -- format "target = hostname:droppath"
	checkline=`echo "${cleanline}" | sed -e 's/^[^=][^=]*=[^:][^:]*:[^:][^:]*$/zzz/p' -e d`

	if [ "${checkline}" = "zzz" ]
	then
		# parse
		targetname=`echo "${cleanline}" | cut -d = -f 1`

		# save the target
		targetlist="${targetlist} ${cleanline}"
		targetchoices="${targetchoices} ${targetname}"
	fi
done < "${conftargets}"

# read the package info
packagelist=""
packagechoices="QUIT "
while read line
do
	# remove comments and whitespace
	cleanline=`echo "${line}" | sed 's/^\([^#]*\)#.*$/\1/g' | sed 's/[ 	]//g'`

	# check the line -- format "packagename = URI|target,target,..."
	checkline=`echo "${cleanline}" | sed -e 's/^[^=][^=]*=[^\|][^\|]*\|[^,][^,]*\(,[^,][^,]*\)*$/zzz/p' -e d`
	if [ "${checkline}" = "zzz" ]
	then
		# parse
		packagename=`echo "${cleanline}" | cut -d = -f 1`

		# save the package
		packagelist="${packagelist} ${cleanline}"
		packagechoices="${packagechoices} ${packagename}"
	fi
done < "${confpackages}"

# jump here for start of build process
# step 1, select package
# step 2, select version and build

currentstep=1

while [ ${currentstep} -ne 0 ]
do

	# step 1, select package
	if [ ${currentstep} -eq 1 ]
	then
		# get the build choice
		echo ""
		echo "Available packages:"
		PS3="Which package would you like to build (1 to quit):"
		select packagechoice in ${packagechoices}
		do
			# did we get an entry
			if [ -n "${packagechoice}" ]
			then
				# was it QUIT
				if [ "${packagechoice}" = "QUIT" ]
				then
					currentstep=0
					continue 2
				else
					# no, so it should be a valid package
					break;
				fi
			fi
		done
		
		# get the build targets
		buildtargets=""
		buildrepo=""
		for packagestring in ${packagelist}
		do
			packagename=`echo "${packagestring}" | cut -d = -f 1`
			packagerepo=`echo "${packagestring}" | cut -d = -f 2 | cut -d '|' -f 1`
			packagetgts=`echo "${packagestring}" | cut -d = -f 2 | cut -d '|' -f 2 | sed 's/,/ /g'`
			if [ "${packagename}" = "${packagechoice}" ]
			then
				buildrepo="${packagerepo}"
				buildtargets="${packagetgts}"
			fi
		done

		# check the repo
		if [ -z "${buildrepo}" ]
		then
			echo "${buildrepo}: error: package repository uri not set"
			exit 1
		fi
		
		# check the targets we found
		for buildtarget in ${buildtargets}
		do
			goodtarget=""
			for targetchoice in ${targetchoices}
			do
				if [ "${targetchoice}" = "${buildtarget}" ]
				then
					goodtarget="true"
					break
				fi
			done
			if [ -z "${goodtarget}" ]
			then
				echo "${myname}: error: unknown build target: ${buildtarget}"
				exit 1
			fi
		done
	fi

	# step 2, select the package and build
	if [ ${currentstep} -eq 1 ]
	then
		echo ""
		echo "Package: ${packagechoice}"
		echo ""
		
		# get the versions
		versiontags=`git ls-remote --tags ${buildrepo} 2>/dev/null | \
			sed -e 's/^.*'${buildtagprefix}'\([0-9][0-9\\.]*\)$/\1/p' -e 'd' | \
			sort -t . -r -n --key=1,1 --key=2,2 --key=3,3 --key=4,4 --key=5,5 --key=6,6 --key=7,7 --key=8,8 --key=9,9 --key=10,10`
		
		# get the version choice
		echo "Available versions:"
		versionchoices="BACK ${versiontags}"
		PS3="Which version would you like to build (1 to go back):"
		select versionchoice in ${versionchoices}
		do
			# did we get an entry
			if [ -n "${versionchoice}" ]
			then
				# was it QUIT
				if [ "${versionchoice}" = "BACK" ]
				then
					currentstep=1
					continue 2
				else
					# no, so it should be a valid version
					break;
				fi
			fi
		done

		echo ""
		echo "You have requested the following build:"
		echo "  Package:    ${packagechoice}"
		echo "  Version:    ${versionchoice}"
		echo "  Repository: ${buildrepo}"
		echo "  Targets:    ${buildtargets}"
		echo ""
		
		echo -n "Please confirm with y/n [n]: "
		read confirm
		if [ "${confirm}" != "y" -a "${confirm}" != "Y" ]
		then
			currentstep=1
			continue
		fi

		# buckets to capture results
		targetresult=""
		buildfail=""
		
		# we have the version chosen to lets start building
		for buildtarget in ${buildtargets}
		do
			buildhost=""
			buildoutdir=""
		
			# find the target 
			for targetitem in ${targetlist}
			do
				targetname=`echo "${targetitem}" | cut -d = -f 1`
				targethost=`echo "${targetitem}" | cut -d = -f 2 | cut -d : -f 1`
				targetoutdir=`echo "${targetitem}" | cut -d = -f 2 | cut -d : -f 2`
				if [ "${buildtarget}" = "${targetname}" ]
				then
					buildhost="${targethost}"
					buildoutdir="${targetoutdir}"
					break
				fi
			done
			if [ -z "${buildhost}" ]
			then
				# should never get here since we already checked all this but just in case
				echo "${myname}: error: unknown build target: ${buildtarget}"
				exit 1
			fi
		
			echo ""
			echo "**************************************************"
			echo "*"
			echo "* Building target ${buildtarget} on host ${buildhost}"
			echo "*"
			echo "**************************************************"
			echo ""
			buildcmd="ssh ${buildhost} ${ANTIPASTI_BIN}/antipasti-buildslave.sh"
			buildcmd="${buildcmd} --repository ${buildrepo}"
			buildcmd="${buildcmd} --version ${versionchoice}"
			buildcmd="${buildcmd} --outdir ${buildoutdir}"
			if [ -z "${buildverbose}" ]
			then
				buildcmd="${buildcmd} > /dev/null"
			else
				buildcmd="${buildcmd} --verbose"
				echo "${myname}: info: remote build command: ${buildcmd}"
			fi		
			if ! ${buildcmd}
			then
				targetresult="${targetresult} ${buildtarget}:FAIL"
				buildfail="true"
				echo "${myname}: error: remote build failed"
			else
				targetresult="${targetresult} ${buildtarget}:SUCCESS"
				buildminsuccess="true"
				echo "${myname}: info: remote build success"
			fi
		done

		# how did we do
		echo ""
		echo "**************************************************"
		echo "*"
		echo "* Build results for ${packagechoice} (${versionchoice})"
		echo "*"
		
		for resultitem in ${targetresult}
		do
			target=`echo "${resultitem}" | cut -d : -f 1`
			result=`echo "${resultitem}" | cut -d : -f 2`
			echo "*   ${target} : ${result}"
		done
		echo "*"
		echo "**************************************************"
		echo ""
		
		if [ -n "${buildfail}" ]
		then
			echo "**************************************************"
			echo "**************************************************"
			echo "**                                              **"
			echo "**                 BUILD FAILED                 **"
			echo "**                                              **"
			echo "**************************************************"
			echo "**************************************************"
		else
			echo "Build completed sucessfully."
		fi

		echo ""
		echo -n "Build another? y/n [n]: "
		read confirm
		if [ "${confirm}" != "y" -a "${confirm}" != "Y" ]
		then
			currentstep=0
			continue
		else
			currentstep=1
			continue
		fi
	fi
done

# is there a post-build command (ie, repository updater)
if [ -n "${buildminsuccess}" -a -n "${buildpostcommand}" ]
then
	repocmd="${buildpostcommand}"
	if [ -n "${buildverbose}" ]
	then
		repocmd="${repocmd} --verbose"
	fi		
	echo ""
	echo "**************************************************"
	echo "*"
	echo "* Running post-build command:"
	echo "*   ${repocmd}"
	echo "*"
	echo "**************************************************"
	echo ""
	if ! ${repocmd}
	then
		echo "${myname}: error: post-build command failed!"
	else
		echo "Post-Build success"
	fi
fi

exit 0
