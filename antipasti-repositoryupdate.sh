#!/bin/bash

#******************************************************************
#**
#**  antipasti repository updater script
#**
#******************************************************************
#** 
#** Copyright (C) 2009 Michael Lasmanis
#** 
#** Permission is hereby granted, free of charge, to any person obtaining
#** a copy of this software and associated documentation files (the 
#** "Software"), to deal in the Software without restriction, including
#** without limitation the rights to use, copy, modify, merge, publish,
#** distribute, sublicense, and/or sell copies of the Software, and to
#** permit persons to whom the Software is furnished to do so, subject to
#** the following conditions:
#** 
#** The above copyright notice and this permission notice shall be
#** included in all copies or substantial portions of the Software.
#** 
#** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
#** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#** 
#** Except as contained in this notice, the name of Michael Lasmanis
#** shall not be used in advertising or otherwise to promote the sale,
#** use or other dealings in this Software without prior written
#** authorization from him.
#** 
#******************************************************************

# what's my name
myname="${0##*/}"


# usage hints
function usage {
	echo "usage: ${myname} [-h | --help] [--verbose] [ --conffile CONFILE ]"
}

# some define
buildconffile="/usr/local/etc/antipasti/antipasti.conf"

# process args

buildverbose=""

while [ "$1" != "" ]; do
    case $1 in
		-h | --help )		usage
							exit 0
 							;;
 		--verbose )			buildverbose="true"
 							;;
		--conffile )		shift
 							if [ -z "$1" ]
 							then
 								echo "${myname}: error: Missing parameter to arg --conffile" 
 								usage
 								exit 1
 							else
								buildconffile=$1
	 						fi
							;;
        * )					echo "error: unknown arg: $1"
							usage
							exit 1
	esac
    shift
done

# source the high level configuration
if [ -f "${buildconffile}" ]
then
	. "${buildconffile}"
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: Failed to load configuration file ${buildconffile}"
		exit 1
	fi
else
	echo "${myname}: error: Failed to locate configuration file ${buildconffile}"
	exit 1
fi

# check conf
confchecks="ANTIPASTI_PACKAGES buildftparchiveconf buildftparchivepooldir buildftparchivedistsdir buildftparchivelistsdir buildftparchivecachedir buildftparchivearchs buildftparchiveorigin buildftparchivelabel buildftparchivedescription"
for confcheck in ${confchecks}
do
	if [ -z "${!confcheck}" ]
	then
		echo "${myname}: error: ${confcheck} not set in conffile"
		exit 1
	fi
done

# check binaries needed
binaries="which apt-ftparchive rm find cut xargs echo gpg"
for binary in ${binaries}
do
	check=`which ${binary}`
	if [ $? -ne 0 ]
	then
		echo "${myname}: error: failed to locate command ${binary}"
		exit 1
	fi
done

# make sure we have the config files
if ! [ -f "${buildftparchiveconf}" ]
then
	echo "error: failed to locate conf file: ${buildftparchiveconf}"
	exit 1;
fi

# switch to the packages root
if ! [ -d "${ANTIPASTI_PACKAGES}" ]
then
	echo "error: package root does not exist: ${ANTIPASTI_PACKAGES}"
	exit 1
fi

# should we sign the releases
buildsignrelease=
if [ -n "${buildgpgkeydir}" -a \
	-d "${buildgpgkeydir}" -a \
	-f "${buildgpgkeydir}/secring.gpg" -a \
	-f "${buildgpgkeydir}/pubring.gpg" -a \
	-f "${buildgpgkeydir}/trustdb.gpg" ]
then
	buildsignrelease="true"
fi

if [ -n "${buildverbose}" ]
then
	echo "info: switching to package root: ${ANTIPASTI_PACKAGES}"
fi
if ! pushd "${ANTIPASTI_PACKAGES}" > /dev/null
then
	echo "error: failed to switch to package root: ${ANTIPASTI_PACKAGES}"
	exit 1
fi

# check the local tree
if [ ! -d "${ANTIPASTI_PACKAGES}/${buildftparchivepooldir}" ]; then
	echo "error: failed to locate pool dir: ${ANTIPASTI_PACKAGES}/${buildftparchivepooldir}"
	exit 1
fi
if [ ! -d "${ANTIPASTI_PACKAGES}/${buildftparchivedistsdir}" ]; then
	echo "error: failed to locate dists dir: ${ANTIPASTI_PACKAGES}/${buildftparchivedistsdir}"
	exit 1
fi
if [ ! -d "${ANTIPASTI_PACKAGES}/${buildftparchivelistsdir}" ]; then
	echo "error: failed to locate dists dir: ${ANTIPASTI_PACKAGES}/${buildftparchivelistsdir}"
	exit 1
fi
if [ ! -d "${ANTIPASTI_PACKAGES}/${buildftparchivecachedir}" ]; then
	echo "error: failed to locate cache dir: ${ANTIPASTI_PACKAGES}/${buildftparchivecachedir}"
	exit 1
fi

# generate the file lists
if [ -n "${buildverbose}" ]
then
	echo "info: generating file lists"
fi
distros=`find "${ANTIPASTI_PACKAGES}/${buildftparchivepooldir}" -mindepth 1 -maxdepth 1 -type d -printf "%P\n"`
distrosections=`find "${ANTIPASTI_PACKAGES}/${buildftparchivepooldir}" -mindepth 2 -maxdepth 2 -type d -printf "%P\n"`
for distrosection in ${distrosections}; do
	distro=`echo "${distrosection}" | cut -d / -f 1`
	section=`echo "${distrosection}" | cut -d / -f 2`
	for packagearch in ${buildftparchivearchs}; do
		bindir="${buildftparchivedistsdir}/${distro}/${section}/binary-${packagearch}"
		if [ ! -d "${ANTIPASTI_PACKAGES}/${bindir}" ]; then
			if [ -n "${buildverbose}" ]
			then
				echo "info: creating missing binary dir ${ANTIPASTI_PACKAGES}/${bindir}"
			fi
			if ! mkdir -p "${ANTIPASTI_PACKAGES}/${bindir}"; then
				echo "error: failed to create ${ANTIPASTI_PACKAGES}/${bindir}"
				exit 1
			fi
		fi
		if [ -n "${buildverbose}" ]
		then
			echo "info: generating file list for ${bindir}"
		fi
		filelist="${ANTIPASTI_PACKAGES}/${buildftparchivelistsdir}/filelist-${distro}-${section}-${packagearch}.txt"
		if ! find "${ANTIPASTI_PACKAGES}/${buildftparchivepooldir}/${distrosection}" -type f "(" -name *_all.deb -o -name *_${packagearch}.deb -o -name *_all_${section}.deb -o -name *_${packagearch}_${section}.deb ")" -printf "${buildftparchivepooldir}/${distrosection}/%P\n" | cut -d / -f 2- > "${filelist}"; then
			echo "error: failed to generate ${filelist}"
			exit 1
		fi
	done
done

# generate the repository
if [ -n "${buildverbose}" ]
then
	echo "info: purging cache"
fi
rm -f ${ANTIPASTI_PACKAGES}/${buildftparchivecachedir}/* 2> /dev/null

# generate the repository
if [ -n "${buildverbose}" ]
then
	echo "info: generating repository"
fi
if [ -z "${buildverbose}" ]
then
	if ! apt-ftparchive generate "${buildftparchiveconf}" > /dev/null 2>&1
	then
		echo "error: repository generation failed"
		exit 1
	fi
else
	if ! apt-ftparchive generate "${buildftparchiveconf}"
	then
		echo "error: repository generation failed"
		exit 1
	fi
fi

# generate the release files
for ourdistro in ${distros}
do
	sections=`find "${ANTIPASTI_PACKAGES}/${buildftparchivepooldir}/${ourdistro}" -mindepth 1 -maxdepth 1 -type d -printf "%P\n" | xargs echo`
	if [ -n "${buildverbose}" ]
	then
		echo "info: generating release file for distro ${ourdistro}"
	fi
	if ! apt-ftparchive \
		-o APT::FTPArchive::Release::Origin="${buildftparchiveorigin}" \
		-o APT::FTPArchive::Release::Label="${buildftparchivelabel}" \
		-o APT::FTPArchive::Release::Description="${buildftparchivedescription}" \
		-o APT::FTPArchive::Release::Suite=${ourdistro} \
		-o APT::FTPArchive::Release::Codename=${ourdistro} \
		-o APT::FTPArchive::Release::Architectures="${buildftparchivearchs}" \
		-o APT::FTPArchive::Release::Components="${sections}" \
		release "${ANTIPASTI_PACKAGES}/${buildftparchivedistsdir}/${ourdistro}" \
		> "${ANTIPASTI_PACKAGES}/${buildftparchivedistsdir}/${ourdistro}/Release" 2> /dev/null
	then
		echo "error: release file generation failed for distro ${ourdistro}"
		exit 1
	fi
	
	# should we sign the release
	if [ -n "${buildsignrelease}" ]
	then
		if [ -n "${buildverbose}" ]
		then
			echo "info: signing release file for distro ${ourdistro}"
		fi
		if ! gpg --homedir "${buildgpgkeydir}" -abs --no-auto-check-trustdb \
			--no-permission-warning --no-random-seed-file -o - \
			"${ANTIPASTI_PACKAGES}/${buildftparchivedistsdir}/${ourdistro}/Release" \
			> "${ANTIPASTI_PACKAGES}/${buildftparchivedistsdir}/${ourdistro}/Release.gpg" 2> /dev/null
		then
			echo "error: release file signing failed for distro ${ourdistro}"
			exit 1
		fi
	fi

done

